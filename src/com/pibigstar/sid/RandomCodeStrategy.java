package com.pibigstar.sid;

public interface RandomCodeStrategy {
    void init();

    int prefix();

    int next();

    void release();
}
