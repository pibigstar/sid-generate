package com.pibigstar.sid.utils;



import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class Ip {

    public static String ip;
    public static long lip;

    static {
        try {
            InetAddress localHostLANAddress = getFirstNonLoopbackAddress();
            ip = localHostLANAddress.getHostAddress();

            byte[] address = localHostLANAddress.getAddress();
            lip =  ((address [0] & 0xFFL) << (3*8)) +
                    ((address [1] & 0xFFL) << (2*8)) +
                    ((address [2] & 0xFFL) << (1*8)) +
                    (address [3] &  0xFFL);
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    private static InetAddress getFirstNonLoopbackAddress() throws SocketException {
        Enumeration en = NetworkInterface.getNetworkInterfaces();
        while (en.hasMoreElements()) {
            NetworkInterface i = (NetworkInterface) en.nextElement();
            for (Enumeration en2 = i.getInetAddresses(); en2.hasMoreElements(); ) {
                InetAddress addr = (InetAddress) en2.nextElement();
                if (addr.isLoopbackAddress()) continue;

                if (addr instanceof Inet4Address) {
                    return addr;
                }
            }
        }
        return null;
    }

}
