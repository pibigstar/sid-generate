package com.pibigstar.sid;

public interface WorkerIdStrategy {
    void initialize();

    long availableWorkerId();

    void release();
}
