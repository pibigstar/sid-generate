## 使用

### 生成固定21位数字字符串

```java
Sid.next()
```

### 生成固定16位的字母数字混编的字符串

```java
Sid.nextShort()
```